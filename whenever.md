<!DOCTYPE html>
<html dir="auto">
<head>
<title>What is Cron?</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
@charset "utf-8";

html { 
	font-size:100%;
	font-family: "Helvetica Neue", "Helvetica", sans-serif;
}
body {
	margin:0;
	padding:1em;
}
body { background: #222; color: #ddd; }
@media (max-device-width: 480px) { 

} 
@media (min-device-width: 481px) { 
	body {
		margin:auto;
		max-width:600px;
	} 
}

blockquote {
	font-style: italic;
}

code, pre {
    border-radius: 3px;
    padding: .5em;
   color: inherit;
}

table {
  margin: 1em 0;
  border: 1px solid #aaa;
  border-collapse: collapse;
}

th {
  padding:.25em .5em;
  border: 1px solid #ccc;  
}

td {
  padding:.25em .5em;
  border: 1px solid #ccc;
}

</style>
</head>
<body>
  <p>What is Cron?</p>

<p>Cron is a job scheduling system available in Linux &amp; MacOS operating systems.</p>

<p>It can be used to run any program at any given time.</p>

<p>This includes Ruby code!</p>

<p>If there is a specific recurring task that you would like to run automatically every day, every week or even every hour, then Cron may be what you’re looking for.</p>

<p>Example of tasks include:</p>

<ul>
<li>Running a weekly database backup</li>
<li>Generating a daily report of website activity</li>
<li>Sending reminder emails</li>
</ul>


<p>Let’s discover how you can make cron work for you!</p>

<h2>Basics Of Cron</h2>

<p>Every user can have a cron file where you define the tasks you want to run.</p>

<p>You can edit this file with the crontab -e command.</p>

<p>Or list scheduled tasks with crontab -l.</p>

<p>Example cron job:</p>

<p>The syntax can be a bit tricky &amp; you may not want to edit these files by hand all the time.</p>

<p>Are there any <a href="https://www.rubyguides.com/2018/09/ruby-gems-gemfiles-bundler/">gems</a> to make things easier for us?</p>

<p>Yes!</p>

<h2>How to Use The Whenever Gem</h2>

<p>Because cron syntax can be hard to remember we can use a gem like whenever.</p>

<p>This gem helps you define cron jobs in Ruby.</p>

<p>Install the gem first.</p>

<p>Then, create the configuration file:</p>

<p>Open it up.</p>

<p>You’ll see some commented-out examples, delete them if you want.</p>

<p>Let’s say we want a backup job that runs every hour.</p>

<p>Write this inside the “config/schedule.rb” file:</p>

<p>Options include:</p>

<ul>
<li>rake (start a <a href="https://www.rubyguides.com/2019/02/ruby-rake/">rake task</a> defined on your current project, note that db:backup isn’t defined by default, but you can try db:version)</li>
<li>runner (Ruby class + method, like Archive.backup_now)</li>
<li>command (<a href="https://www.rubyguides.com/2018/12/ruby-system/">system</a> command)</li>
</ul>


<p>Now:</p>

<p>Run the whenever &ndash;update-crontab command to generate the cron entries.</p>

<p>You should be able to see your new entry with crontab -l.</p>

<p>This is what I got:</p>

<p>Notice that the task will run in production mode.</p>

<p>Want to change that?</p>

<p>You can use this setting:</p>

<p>This goes in your schedule.rb file, at the top.</p>

<p>You can also use this command:</p>

<p>This creates all the tasks with “development” as the <a href="https://www.rubyguides.com/2019/01/ruby-environment-variables/">environment</a> when you run this command.</p>

<h2>More Whenever Examples</h2>

<p>This example shows how you can be more specific.</p>

<p>Weekly task:</p>

<p>This example shows how to run the same task multiple times during the day.</p>

<p>Run task twice a day:</p>

<h2>Whenever Gem Logging &amp; Troubleshooting</h2>

<p>Once you have tasks in your crontab file (check with crontab -l) you’re ready to go.</p>

<p>Next:</p>

<p>How do you know if the scheduled tasks are working correctly?</p>

<p>You can set up logging &amp; read the output.</p>

<p>Enable logging by adding this at the top of your schedule.rb file.</p>

<p>Configuration:</p>

<p>Remember to run the whenever &ndash;update-crontab command every time you make changes!</p>

<p>Log file still empty?</p>

<p>You’ll have to wait the scheduled time to see something happen.</p>

<p>Btw, if you see this error in the logs:</p>

<p>That means cron isn’t running the same version of Ruby that you’re.</p>

<p>If you’re using a Ruby version manager:</p>

<p>Make sure to load it from the .bash_profile file, instead of .bashrc, so your cron tasks can use it.</p>

<h2>Alternatives to Whenever</h2>

<p>Whenever is a well-maintained gem &amp; it works great.</p>

<p>But what are the alternatives?</p>

<p>Here’s a list:</p>

<ul>
<li><a href="https://elements.heroku.com/addons/scheduler">Heroku Scheduler</a>, it’s a free job scheduling add-on for Heroku users. Not as flexible as cron itself but it gets things done</li>
<li><a href="https://github.com/jmettraux/rufus-scheduler">Rufus Scheduler</a>, a Ruby gem that replaces cron with an in-memory scheduler</li>
<li><a href="https://github.com/mattyr/sidecloq">Sidecloq</a> &amp; <a href="https://github.com/ondrejbartas/sidekiq-cron">Sidekiq-cron</a>, both are Sidekiq add-ons that add recurring tasks to Sidekiq</li>
</ul>


<p>Note that both Sidekiq addons want actual cron syntax for scheduling, so it won’t be as nice as whenever syntax.</p>

<p>The good thing is that they integrate with Sidekiq web UI.</p>

<h2>Summary</h2>

<p>You’ve learned how to schedule cron jobs using the whenever gem in Ruby!</p>

<p>Please share this article so more people can benefit from it.</p>

<p>Thanks for reading.</p>
</body>
</html>